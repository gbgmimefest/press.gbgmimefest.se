---
title: Ika Nord är klar för Mimefest 2018
description: Ika är glad.
tags:
  - '2018'
  - Storan
date: '2018-08-25'
author: Agneta Leander
language: sv
---
Ika Nord fick klassisk balettundervisning i Halmstad från det hon var sex år tills hon var sjutton. Ett par år senare började hon studera i Paris på Conservatoire National de l’Art du Mime, därefter Matt Mattox Jazz Art Dance School, École de Mime Étienne Decroux och L’Atelier de Théâtre Robert Cordier.

Ika Nord arbetar och framträder som artist inom många olika områden. Med Halmstads Teater-ensemble har hon gjort Puck i En midsommarnattsdröm, Viola i Trettondagsafton och Claire i Jungfruleken, hon har framträtt i roller i Barcelona, Paris och Amsterdam, spelat gatuteater i ett flertal europeiska städer, undervisat på till exempel Rigas nationalteater och École Nationale de l’Art du Mime i Paris.

Nord har också arbetat som regissör och koreograf, bland annat SVT och Berns Salonger, och exempel på helt egna produktioner är "Every man and every woman is a star", "Teaterkalas med Ika och Åke" och "Apan". Hon har samarbetat med bland annat Göteborgssymfonikerna och Musik i Väst i olika musikföreställningar. 1993 fick hon svenska teaterkritikernas barn- och ungdomspris för rollen som katten Findus i TV:s julkalender "Tomtemaskinen". Mest känd är hon nog som den livliga och påhittiga tjejen i "Ika i rutan".

Ika Nord verkar idag som skådespelare, mimartist, clown och regissör.