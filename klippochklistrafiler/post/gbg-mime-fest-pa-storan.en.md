+++
title = "Gbg Mime Fest in Storan Theatre"
description = ""
tags = [
    "2018",
    "Storan",
]
date = "2018-08-22"
author = "Agneta Leander"
language = "en"
+++

In the month of october, several gifted mime artists will do amazing stuff, entirely without props. Good for them. And for us.
