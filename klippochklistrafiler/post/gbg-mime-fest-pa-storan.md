---
title: Gbg Mime Fest på Storan
description: Hela oktober och november brakar mimfesten loss.
tags:
  - 2018
  - Storan
date: '2018-08-22'
author: Agneta Leander
language: sv
---

## Hela oktober och november brakar mimfesten loss. 

*Peter Harrysson* kommer gå emot vinden och __Leif Pagrotsky__ drar i ett osynligt rep. Samtliga förskolor i Västra Götaland har redan köpt biljett.
