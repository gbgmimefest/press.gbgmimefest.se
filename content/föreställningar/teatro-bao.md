---
title: Teatro Baó
description: Teatro Baó gör magie nouvelle.
tags:
  - 2018
date: 2018-09-06
author: Marcus Gustafsson
language: sv
---

Teatro Baó skapar föreställningar i en genre inom nycirkusen som inte finns representerad i Sverige. I Frankrike kallas denna typ av scenkonst för magie nouvelle (nymagi), och lanserades av Victoria Chaplin, dotter till Charlie Chaplin. Formen har förfinats och sedan spridit sig över världen som poetiska, visuellt mycket starka och inte sällan komiska föreställningar där magin är ständigt närvarande.
Teatro Baó består av Marta Chaves och Magnus Jakobsson.

## Praktisk info

**Längd:** 40 min \
**Målgrupp:** Familj

## Länkar

- **Officiell hemida:** [teatrobao.com](https://teatrobao.com)
