---
title: The Doggy Style Project
description: The Doggy Style project is the final chapter in BRAVO TOGAs ”Slut-shaming trilogy”.
tags:
  - 2018
date: 2018-09-21
author: Joel Zandén
language: sv
---

Föreställningen dissekerar och hyllar bilder av slampan och genom en koreografisk och dramatisk förflyttning mellan sexuellt objekt, subjekt och kanske något tredje. Med föreställningen önskar BRAVO TOGA att omfamna ”hundstilens” tvetydiga symbolik mellan erotisk laddning och praktisk undersökning.

## Praktisk info

**Spelplats:** Caféscenen
**Längd:** 1h 30 min
**Målgrupp:** Vuxna från 18 år

## Länkar

- **Officiell hemsida:** [www.bravotoga.com/doggy-style-project](http://www.bravotoga.com/doggy-style-project.html)
