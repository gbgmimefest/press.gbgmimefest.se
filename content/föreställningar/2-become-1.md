---
title: 2 become 1
description: 2 Become 1 är en fysiskt vibrerande föreställning skapad och framförd av Jonna Ljunggren och Li Molnár Kronlid med musik av Niki Yrla.
tags:
  - 2018
date: 2018-09-06
author: Marcus Gustafsson
language: sv
---

2 Become 1 är en fysiskt vibrerande föreställning skapad och framförd av Jonna Ljunggren och Li Molnár Kronlid med musik av Niki Yrla. Verket utspelar sig i ett flickrum och med fokus på hur Spice Girls känns, som sprakande lust i kroppen, utforskas sexualitet, femininitet och makt i ett rum där allt är tillåtet. Föreställningen hade premiär under Departurefestivalen på Stockholms Dramtiska Högskola, oktober 2016 och har efter dess bland annat framförts på Scenkonstgalan på Storan i Göteborg 2017.

## Citat

”Vi sitter här på rummet och speglar oss i cd-skivor. Dom tittar på oss. Vi tittar på dom. Vi vill ta oss in i deras kroppar. D.v.s. vara och knulla Spice Girls. Vi tar på oss läppstift och nagellack. Vi tittar på varandra. Dom tittar på oss genom oss. Vi klär av varandra kläderna och sätter på varandra andra kläder. \
P.s. Come a little bit closer baby d.s."

## Praktisk info

**Längd:** 30 min \
**Målgrupp:** Från 16 år

## Länkar

- **Trailer:** [https://vimeo.com/185614504](https://vimeo.com/185614504)
- **Scenkonstguiden:** [http://scenkonstguiden.se/event/2-become-1-2/](http://scenkonstguiden.se/event/2-become-1-2/)
- **gbgmimefest.se:** [https://gbgmimefest.se/2-become-1/](https://gbgmimefest.se/2-become-1/)

{{< vimeo 185614504 >}}
