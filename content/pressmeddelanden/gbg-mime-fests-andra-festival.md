---
title: Gbg Mime Fest laddar för ytterligare en festival
description: Gbg Mime Fest laddar för ytterligare en festival
tags:
  - 2018
date: 2018-09-18
author: Agneta Leander
language: sv
---

**Den nystartade mimfestivalen Gbg Mime Fest kommer i år att gå av stapeln den 2--4 november på Teater Aftonstjärnan på Hisingen. "Det är den vackraste teater jag varit i", säger Hilda Rydman, initiativtagare till festivalen. "Kanske är det en extra väg att ta sig uppför berget men det kommer vara värt det."**

"Det känns jätteroligt och jätteviktigt" fortsätter Hilda Rydman. "Att vi fått stöd från Kulturrådet och Göteborgs stad tyder på längtan efter den rörelsebaserade scenkonsten." Festivalen startades 2017 så detta är festivalens andra år. Antalet akter har i år utökats och det kommer finnas möjlighet att se föreställningar ifrån Spanien, Finland och Sverige. Biljetter släpps inom kort.

Festivalen vänder sig till alla i Göteborg från två år och uppåt. "Jag hoppas att vi öppnar upp för den rörelsebaserade scenkonsten i Göteborg" säger Hilda Rydman. "Och jag hoppas att besökaren kan få med sig det där lilla extra. Det fantastiska, poetiska, vackra eller fula. Det där som går rakt in i hjärtat utan att du kan förklara".

För mer information och biljetter se årets festivalhemsida: [gbgmimefest.se](https://gbgmimefest.se).

## Kontakt

Agneta Leander, pressansvarig \
[073-669 85 60](tel:+46736698560) \
[info@agnetaleander.com](mailto:info@agnetaleander.com)

## Pressbilder

{{< figure src="https://gbgmimefest.se/wp-content/uploads/2018/08/cropped-fb-profil-4.png" link="https://gbgmimefest.se/wp-content/uploads/2018/08/cropped-fb-profil-4.png" title="Logotyp för Gbg Mime Fest" alt="Logotyp för Gbg Mime Fest" >}}

{{< figure src="https://gbgmimefest.se/wp-content/uploads/2017/10/Hilda-Rydman-kopia.jpg" link="https://gbgmimefest.se/wp-content/uploads/2017/10/Hilda-Rydman-kopia.jpg" title="Initiativtagare Hilda Rydman" alt="Initiativtagare Hilda Rydman" >}}

{{< figure src="https://gbgmimefest.se/wp-content/uploads/2018/09/20180213_0144.jpg" link="https://gbgmimefest.se/wp-content/uploads/2018/09/20180213_0144.jpg" title="Allt som finns av Teater Tre" alt="Allt som finns av Teater Tre" >}}
