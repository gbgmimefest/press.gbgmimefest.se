---
title: Mim tar plats i Göteborg
description: Mim tar plats i Göteborg
tags:
  - 2018
date: 2018-10-21
author: Agneta Leander
language: sv
---

**Med en knapp månad kvar till start har [GBG Mime Fest](https://gbgmimefest.se) äntligen släppt biljetter och spelschema för årets festival. "Jag tycker att festivalen visar sin styrka genom att erbjuda föreställningar för alla åldrar", säger Josephine Gray, artist i kompaniet [Iraqi Bodies](https://gbgmimefest.se/visit-to-gerontion-2). "Festivalen undviker att profilera sig mot en specifik åldersgrupp vilket gör att den håller fast vid mimens universella språk som har varit centralt för teaterarbetet sedan Euripides."**

GBG Mime Fest är en festival för rörelsebaserad scenkonst och 2018 är festivalens andra år. Antalet akter har utökats och flera internationella artister är på plats, hitresta bland annat från Spanien. "Vi har handplockat föreställningar för att visa på mimens otroliga bredd och kraft", säger Hilda Rydman, festivalens initiativtagare. "Några föreställningar är för barn och några är barnförbjudna. Några är helt utan ord medan andra är fulla av ord. Det gemensamma är att alla artister har mimen som grund i sitt arbete."

En av många akter som utmärker sig är ["The Doggy Style project"](https://gbgmimefest.se/the-doggy-style-project), ett samarbetsprojekt mellan scenkonstnärer från Finland, Danmark och Sverige. Det är en föreställning med 16-årsgräns där konstnärerna gör upp med den förutfattade bilden av den kvinnliga slampan.

"Rätta mig om jag har fel men jag tror att GBG Mime Fest är den enda mimfestivalen i hela Skandinavien – och det är verkligen på tiden att vi får en!" säger Josephine Gray. "Idag när man pratar om mim tänker de flesta människor på pantomim. Det är visserligen en del av mimen men det finns så mycket mer!" \
"Mimen öppnar upp vårt samhälle då det är en konstform som kan upplevas över olika gränser. Det är ett hantverk vars uppgift är att bortom orden beröra sin publik", tillägger Hilda Rydman.

För mer information och biljetter se årets festivalhemsida: [gbgmimefest.se](https://gbgmimefest.se).

## Kontakt

Agneta Leander, pressansvarig \
[073-669 85 60](tel:+46736698560) \
[info@agnetaleander.com](mailto:info@agnetaleander.com)

## Pressbild

{{< figure src="https://gbgmimefest.se/wp-content/uploads/2018/09/2.jpg" link="https://gbgmimefest.se/wp-content/uploads/2018/09/2.jpg" title="The legend of the faun and the journey. Photo: Jose Angel Ribas" alt="The legend of the faun and the journey" >}}
