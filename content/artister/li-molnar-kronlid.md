---
title: Li Molnár Kronlid
description: Li Molnár Kronlid
tags:
  - 2018
  - Li Molnár Kronlid
date: 2018-09-06
author: Marcus Gustafsson
language: sv
---

Li Molnár Kronlid har studerat mimskådespeleri på Stockholms Dramatiska Högskola. Lis har bakgrund inom koreografi och dans. Hon jobbar utifrån egna erfarenheter och har ett förhållningssätt till arbetet där Lust är centralt, i ordets dubbla bemärkelse.
