---
title: Jonna Ljunggren
description: Jonna Ljunggren
tags:
  - 2018
  - Jonna Ljunggren
date: 2018-09-06
author: Marcus Gustafsson
language: sv
---

## Jonna Ljunggren

Jonna Ljunggren studerade mimskådespeleri på Stockholms Dramatiska Högskola. Utöver mimhögskolan har Jonna erfarenhet av skådespeleri och bildkonst vilket resulterar i en bred scen-konstnärligt inriktning med mimskådespeleri som bas. Hon jobbar ofta utifrån egna erfarenheter och har ett förhållningssätt till att arbetet ska utgå från Lust, i ordets dubbla bemärkelse.
